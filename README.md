Welcome to Infinity Engine Games!

This GitLab project was created in order to help users find mods that across multile platforms. This project is currently in the early stages of development and currently I am the only one managing this database. 

This project currently only links to either the git repository, like GitHub, or the website where the file is hosted. You cannot download the file here or submit issues to those projects here, as they will not be read or viewed. As this is an ongoing project you may notice things could change quite a bit. Also, if a mod no longer appears in this database please be advised it could be for one of two reasons; the git repository was deleted (as all git repositories here are mirrored to the original) or the author has requested that their mods not appear in this database. 

The purpose of this database is two fold. 
    1) To increase the awareness of what mods are avaliable. There are many websites with mods including beamdogs own forums for which mods are lost. Having a database that is open source and community driven should help increase the exposure of singular modders and modders from different websites.
    2) To move the community to a single platform for modding. GitHub is currently the most widely used, but any git repository where the community can help keep Infinity Engine style games alive is key. Mod authors come and go, sometimes their work goes unfinished for a variety of reasons. Having a place where someone else could pick that up is vital to the success of the modding community and games at large. 
    
Please feel free to submit issues about broken links, incorrect mod authors, or any other issues you encounter here on this Suggestion Board. 

You can add an issue by clicking the "Issue" button the side of this webpage and clicking "New issue". 

You can also see that there is a wiki option available. I'm hoping to eventually use that as a springboard for more information that has been collected throughout 20 years of the Infinity Engine being around. If you have a suggestion, please add it so we can have a thriving wiki. If you would like to be a contributor, please contact me.